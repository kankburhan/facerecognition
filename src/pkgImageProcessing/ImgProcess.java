/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgImageProcessing;

import java.awt.image.BufferedImage;

/**
 *
 * @author Puji Astuti
 */
public class ImgProcess {

    public double[][] RGB(BufferedImage end, int lebar) {
        double[][] dataRGB = new double[lebar * 3][3];
        int counter = 0;
        //System.out.println("Red");
        for (int y = 0; y < end.getHeight(); y++) {
            for (int x = 0; x < end.getWidth(); x++) {
                int rgb = end.getRGB(x, y);
                int red = rgb & 0x000000FF;
                dataRGB[counter][0] = red;
                //System.out.print(red + "    ");
                counter++;
            }
            //System.out.println();
        }
        //System.out.println("Green");
        for (int y = 0; y < end.getHeight(); y++) {
            for (int x = 0; x < end.getWidth(); x++) {
                int rgb = end.getRGB(x, y);
                int green = (rgb & 0x0000FF00) >> 8;
                //System.out.print(green + "    ");

                dataRGB[counter][1] = green;
                counter++;
            }
            // System.out.println();
        }
        //System.out.println("Blue");
        for (int y = 0; y < end.getHeight(); y++) {
            for (int x = 0; x < end.getWidth(); x++) {
                int rgb = end.getRGB(x, y);
                int blue = (rgb & 0x00FF0000) >> 16;
                //System.out.print(blue + "    ");
                dataRGB[counter][2] = blue;
                counter++;
            }
            //System.out.println();
        }
        //System.out.println("");
        return dataRGB;
    }
    
    

    public BufferedImage GRAY(BufferedImage end) {
        BufferedImage dest = new BufferedImage(end.getWidth(), end.getHeight(), BufferedImage.TYPE_INT_RGB);
        System.out.println("");
        System.out.println("======");
        System.out.println("*GRAY*");
        System.out.println("======");
        for (int y = 0; y < end.getHeight(); y++) {
            for (int x = 0; x < end.getWidth(); x++) {
                int rgb = end.getRGB(x, y);
                int alpha = (rgb << 24) & 0xFF;
                int red = rgb & 0x000000FF;
                int green = (rgb & 0x0000FF00) >> 8;
                int blue = (rgb & 0x00FF0000) >> 16;
                int avg = (red + green + blue) / 3;
                System.out.print(avg + "    ");
                int gray = alpha | avg << 16 | avg << 8 | avg;
                dest.setRGB(x, y, gray);
            }
            System.out.println();
        }
        System.out.println("");
        return dest;
    }
}
