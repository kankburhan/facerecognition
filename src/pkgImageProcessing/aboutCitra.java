/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgImageProcessing;

import java.awt.image.BufferedImage;

/**
 *
 * @author Puji Astuti
 */
public class aboutCitra {
    BufferedImage B;
    public void setImage(BufferedImage B){
        this.B=B;
        //invalidate();
    }
   
    public int getTinggi(BufferedImage B){
        return B.getHeight();
    }
    
    public int getLebar(BufferedImage B){
        return B.getWidth();
    }

    public int getLebar(BufferedImage[] bi, int pos) {
        return bi[pos].getWidth();
    }

    public int getTinggi(BufferedImage[] bi, int i) {
         return bi[i].getHeight();
    }
}
