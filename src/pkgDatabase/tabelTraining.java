/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgDatabase;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author me.pujiastuti
 */
public class tabelTraining extends AbstractTableModel {

    private List<dtTraining> list = new ArrayList<dtTraining>();

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return list.get(rowIndex).getIterasi();
            case 1:
                return list.get(rowIndex).getSSE();
            default:
                return null;
        }
    }

    public String getColumnName(int kolom) {
        switch (kolom) {
            case 0:
                return "Iterasi";
            case 1:
                return "SSE Dicapai";
            default:
                return null;
        }
    }
    
    public void add(dtTraining train){
        list.add(train);
        fireTableRowsInserted(getRowCount(), getColumnCount());
    }
    
    public void delete(int i, int baris){
        list.remove(i);
        fireTableRowsDeleted(i, baris);
    }
    
    public dtTraining get(int baris){
        return (dtTraining) list.get(baris);
    }
}
