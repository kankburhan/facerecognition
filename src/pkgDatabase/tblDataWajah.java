/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgDatabase;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Puji Astuti
 */
public class tblDataWajah extends AbstractTableModel {

    private List<dtMasterWajah> list = new ArrayList<dtMasterWajah>();

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return list.get(rowIndex).getKdPelatihan();
            case 1:
                return list.get(rowIndex).getNama();
            case 2:
                return list.get(rowIndex).getTanggal();
            case 3:
                return list.get(rowIndex).getAlamat();
            case 4:
                return list.get(rowIndex).getOutput();
            default:
                return null;
        }
    }

    public String getColumnName(int kolom) {
        switch (kolom) {
            case 0:
                return "Kode Pelatihan";
            case 1:
                return "Nama";
            case 2:
                return "Tanggal lahir";
            case 3:
                return "Alamat";
            case 4:
                return "Target Keluaran";
            default:
                return null;
        }
    }

    public void add(dtMasterWajah wajah) {
        list.add(wajah);
        fireTableRowsInserted(getRowCount(), getColumnCount());
    }

    public void delete(int i, int baris) {
        list.remove(i);
        fireTableRowsDeleted(i, baris);
    }

    public dtMasterWajah get(int baris) {
        return (dtMasterWajah) list.get(baris);
    }
}
