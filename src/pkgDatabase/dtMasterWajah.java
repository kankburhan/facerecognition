/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgDatabase;

import java.util.Date;

/**
 *
 * @author Puji Astuti
 */
public class dtMasterWajah {
    private String kdPelatihan, nama, alamat, output;
    Date tanggal;
    
    public String getKdPelatihan(){
        return kdPelatihan;
    }
    
    public void setKdPelatihan(String kdPelatihan){
        this.kdPelatihan=kdPelatihan;
    }
    
    public String getNama(){
        return nama;
    }
    
    public void setNama(String nama){
        this.nama = nama;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }
        
    public String getAlamat(){
        return alamat;
    }
    
    public void setAlamat(String alamat){
        this.alamat = alamat;
    }
    public String getOutput(){
        return output;
    }
    
    public void setOutput(String output){
        this.output = output;
    }
}
