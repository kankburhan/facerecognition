/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgDatabase;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author me.pujiastuti
 */
public class tabelWajah extends AbstractTableModel {

    private List<dtWajah> list = new ArrayList<dtWajah>();

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return list.get(rowIndex).getIdPelatihan();
            case 1:
                return list.get(rowIndex).getNama();
            case 2:
                return list.get(rowIndex).getEig1();
            case 3:
                return list.get(rowIndex).getEig2();
            case 4:
                return list.get(rowIndex).getEig3();
            case 5:
                return list.get(rowIndex).getTaget();
            default:
                return null;
        }
    }

    public String getColumnName(int kolom) {
        switch (kolom) {
            case 0:
                return "Kode Pelatihan";
            case 1:
                return "Nama";
            case 2:
                return "Eigen 1";
            case 3:
                return "Eigen 2";
            case 4:
                return "Eigen 3";
            case 5:
                return "Target Pelatihan";
            default:
                return null;
        }
    }
    
    public void add(dtWajah wajah){
        list.add(wajah);
        fireTableRowsInserted(getRowCount(), getColumnCount());
    }
    
    public void delete(int i, int baris){
        list.remove(i);
        fireTableRowsDeleted(i, baris);
    }
    
    public dtWajah get(int baris){
        return (dtWajah) list.get(baris);
    }
}
