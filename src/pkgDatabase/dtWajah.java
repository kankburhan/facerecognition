/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgDatabase;


/**
 *
 * @author me.pujiastuti
 */
public class dtWajah{
   private String idPelatihan, idCitra, nama, gambar, eig1, eig2, eig3, taget;

    public String getIdPelatihan() {
        return idPelatihan;
    }

    public void setIdPelatihan(String idPelatihan) {
        this.idPelatihan = idPelatihan;
    }

    public String getIdCitra() {
        return idCitra;
    }

    public void setIdCitra(String idCitra) {
        this.idCitra = idCitra;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getEig1() {
        return eig1;
    }

    public void setEig1(String eig1) {
        this.eig1 = eig1;
    }

    public String getEig2() {
        return eig2;
    }

    public void setEig2(String eig2) {
        this.eig2 = eig2;
    }

    public String getEig3() {
        return eig3;
    }

    public void setEig3(String eig3) {
        this.eig3 = eig3;
    }

    public String getTaget() {
        return taget;
    }

    public void setTaget(String taget) {
        this.taget = taget;
    }
  
}
