/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgDatabase;

import com.mysql.jdbc.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Puji
 */
public class insertBobot {

    dbKoneksi a = new dbKoneksi();
    public Statement stt;
    public Connection conn;
    
    public  double getMaksimal(){
        double mak = 0;
        try {
            conn = a.koneksiDb();
            stt = conn.createStatement();
            //a.koneksiDb();
            String sql = "SELECT MAX(t.mak) AS maksimal " 
                +"FROM (SELECT ROUND(SPLIT_STRING (mxGambar, '#', 1), 3) AS mak "
                +"FROM tb_citrawajah UNION ALL SELECT ROUND(SPLIT_STRING (mxGambar, '#', 2), 3) AS mak " 
                +"FROM tb_citrawajah UNION ALL SELECT ROUND(SPLIT_STRING (mxGambar, '#', 3), 3) AS mak "
                +"FROM tb_citrawajah) t";
            ResultSet rs = stt.executeQuery(sql);
            while (rs.next()) {
                mak = Double.parseDouble(rs.getString("maksimal"));
            }
            conn.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Gagal menjalankan Query", "Eksekusi Query", JOptionPane.WARNING_MESSAGE);
        }
        return mak;
    }

    public void simpanBobotAwal(String jnsBobot, double bobot) {
        try {
            conn = a.koneksiDb();
            stt = conn.createStatement();
            //a.koneksiDb();
            String sql = "insert into tb_bobot values('"
                    + jnsBobot + "','"
                    + bobot + "')";
            stt.executeUpdate(sql);
            conn.close();
        } catch (SQLException e) {
            try {
                conn = a.koneksiDb();
                stt = conn.createStatement();
                //a.koneksiDb();
                String sql = "UPDATE tb_bobot SET nilaiBobot = '" + bobot + "'"
                        + " where idBobot = '" + jnsBobot + "'";
                stt.executeUpdate(sql);
                conn.close();
            } catch (SQLException eq) {
                System.out.println("Error update data " + eq);
            }
        }
    }

    public double readBobot(String idBobot) {
        double bobot = 0;
        try {
            conn = a.koneksiDb();
            stt = conn.createStatement();
            String sql = "SELECT * FROM tb_bobot where idBobot='" + idBobot + "'";
            ResultSet rs = stt.executeQuery(sql);
            while (rs.next()) {
                bobot = Double.parseDouble(rs.getString("nilaiBobot"));
            }
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Gagal pembacaaan Bobot " + ex);
        }
        return bobot;
    }

    public void saveSetting(String idSetting, String nilai) {
        try {
            conn = a.koneksiDb();
            stt = conn.createStatement();
            //a.koneksiDb();
            String sql = "insert into tb_settingjst values('"
                    + idSetting + "','"
                    + nilai + "')";
            stt.executeUpdate(sql);
            conn.close();
        } catch (SQLException e) {
            System.out.println("Error simpan " + e);
            conn = a.koneksiDb();
            try {
                stt = conn.createStatement();
                String sql = "UPDATE tb_settingjst set nilai = '" + nilai + ""
                        + "' where idSetting  = '" + idSetting + "'";
                stt.executeUpdate(sql);
                conn.close();
            } catch (SQLException eq) {
                System.out.println("Error Update data" + eq);
            }
        }

    }

    public void clearBobot() {
        try {
            conn = a.koneksiDb();
            stt = conn.createStatement();
            //a.koneksiDb();
            String sql = "DELETE FROM tb_bobot";
            stt.executeUpdate(sql);
        } catch (SQLException e) {
            System.out.println("Error Clear " + e);
        }
    }
    
    public ResultSet bacaWajah(String T) {
       ResultSet rs = null;
        try {
            conn = a.koneksiDb();
            stt = conn.createStatement();
            String sql = "SELECT * FROM tb_pelatihan as a, tb_citrawajah as b where a.idPelatihan = b.idPelatihan and a.nilaiTarget='" + T + "'";
            rs = stt.executeQuery(sql);
            
        } catch (SQLException ex) {
            System.out.println("Gagal pembacaaan Bobot " + ex);
        }
        return rs;
    }
}